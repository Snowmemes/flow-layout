
#include <bits/stdc++.h>
using namespace std;

int main() {
	while (1) {
		int width;
		cin >> width;

		if ( width == 0 ) {
			break;
		}

		int pw = 0, cw = 0, ph = 0, ch = 0;

		while (1) {
			int w, h;
			cin >> w >> h;

			//cout << "  got " << w << "x" << h << endl;

			if ( cw + w > width ) {
				ph = ch;
				cw = w;
			} else {
				cw += w;
			}

			ch = max( ch, ph + h );
			pw = max( pw, cw );

			if ( w == -1 && h == -1 ) {
				ph = ch;
				break;
			}

			//cout << "  ch:" << ch << "  cw:" << cw << "  ph:" << ph << "  pw:" << pw << endl;
		}

		cout << pw << " x " << ph << endl;
	}

	return 0;
}
